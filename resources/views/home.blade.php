@extends('parent')
<style>
    .dropdown-toggle::after {
        display: none;
    }

    .text-decoration-none {
        text-decoration: none !important;
    }

    /* Font Awesome Icons have variable width. Added fixed width to fix that.*/
    .icon-width {
        width: 2rem;
    }

    .navbar-nav li:hover .dropdown-menu {
        display: block;
    }
</style>
@section('main')
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Tripegate</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php 
                    $menu2 = $menus;
                    $menuhtml = "";
                    function drawmenu($cn, $menu1, $indexid){
                        $url = $menu1[$indexid]->url;
                        $menu3 = $menu1;
                        //<li class="active"><a href="#">Display</a></li>
                        $text = '<li ';
                        if($cn == $menu1[$indexid]->display_name)
                            $text .= 'class="dropdown active"';
                        $text .= '><a href="'. url($url) .'">'.$menu1[$indexid]->display_name .'</a>';
                        
                        $menuid = $menu1[$indexid]->id;
                        $start = true;
                        foreach($menu1 as $key => $value){
                            if($menuid == $value->parent_id){
                                if($start) {
                                    $text .= '<ul  class="dropdown-menu">';
                                    $start = false;
                                }
                                $text .= drawmenu($cn, $menu3, $key);
                            }
                        }
                        if($start === false)
                            $text .= '</ul>';
                        return $text . "</li>";
                    }
                    foreach($menus as $key => $value){
                        if($value->parent_id == 0)
                            $menuhtml .= drawmenu($controller_name, $menu2, $key);
                    }
                    echo $menuhtml;
                ?>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>

<div class="container">



</div> <!-- /container -->
@endsection