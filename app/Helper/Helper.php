<?php
namespace App\Helper;
use Cookie;

class Helper{
    public static  function setCookie($name, $value, $duration) {
        Cookie::queue($name, $value, $duration);
        return true;
    }

    public static  function getCookie($name){
        return Cookie::get($name);
    }
}