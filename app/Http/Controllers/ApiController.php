<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Redirect;
use Cookie;
use DB;
use View;

class ApiController extends Controller {

    protected $menuModel;
    public function __construct(Request $request){
        $this->menuModel = new \App\Menu;
    }

    public function login()
    {
        return view('signin');
    }

    public function signin( Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'user' => 'required|email',
                'pass' => 'required',
            ]);
    
            if ($validator->fails()) {
                return Redirect::to('login')
                                ->withErrors($validator)
                                ->withInput(Input::except('password')); 
            }else{
                $user = $request->input('user');
                $pass = $request->input('pass');
                $client = new Client();
                $res = $client->request('POST', 'http://34.229.200.81/api/auth/sign-in', [
                    'form_params' => [
                        'email' => $user,
                        'password' => $pass,
                    ]
                ]);
                
                if($res->getStatusCode() === 200){
                    $data           =  $res->getBody();
                    $response       = json_decode($data,TRUE);
                    $access_token   = $response['access_token'];
                    $access_token_type = $response['token_type'];
                    $access_token_duration = $response['expires_in'];

                    $userid         = $response['user']['id'];
                    $first_name     = $response['user']['first_name'];
                    $last_name      = $response['user']['last_name'];
                    $email          = $response['user']['email'];
                    $username       = $response['user']['username'];
                    $account_type   = $response['user']['account_type'];

                    // \Helper::setCookie('access_token', $access_token, $access_token_duration );
                    // \Helper::setCookie('userid', $userid, $access_token_duration );
                    // \Helper::setCookie('logged', 1, $access_token_duration );

                    $cookie =  Cookie::make('logged', 1, $access_token_duration);
                    return Redirect::to('home');
                }else{
                    return Redirect::to('login');
                }
    
            }
        } catch (\Throwable $th) {
            return Redirect::to('login');
        }
    }

    public function home(Request $request){
        // echo dd(Cookie::get('logged'));
        // $menus = $this->menuModel->all();
        // var_dump($menus);
        
        $results = DB::select('select * from v_menu_permission where p_id = ?', [1]);
        return View::make("home")->with(array('menus'=>$results, 'controller_name'=>'Home'));

        // try {
        //     if(Cookie::has('logged') && Cookie::get('logged') === 1){
        //         $results = DB::select('select * from v_menu_permission where p_id = ?', [1]);
        //         return View::make("home")->with(array('menus'=>$results));
        //     }else{
        //         return Redirect::to('login');
        //     }
        // } catch (\Throwable $th) {
        //     return Redirect::to('login');
        // }
    }
}